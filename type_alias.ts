type CarYear = number;
type CarType = string;
type CarModel = string;

type Car = {
    year: CarYear,
    type: CarType,
    model: CarModel
}

const carYear: CarYear = 2001;
const carType: CarType = "Ford";
const carModel: CarModel = "Ranger";

const car1: Car = {
    year: 2001,
    type: "Raptor",
    model: "XXX"
}

console.log(car1);
